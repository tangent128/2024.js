import { IslandGrid } from "./island/grid";
import { renderIslands } from "./island/render";
import { canvas2d, h } from "./lib/html";

const BLOWUP = 4;
const WIDTH = 240;
const HEIGHT = 135;

const DEFAULT_SEED = 128;

export function IslandApplet() {
  // STATE

  let timerId: number;
  let ticks = 0;
  let islands = new IslandGrid(WIDTH, HEIGHT, DEFAULT_SEED);

  // UI

  const [canvas, cx] = canvas2d({
    width: WIDTH * BLOWUP,
    height: HEIGHT * BLOWUP,
  });
  cx.scale(BLOWUP, BLOWUP);

  const seedInput = h("input", { type: "number", valueAsNumber: DEFAULT_SEED });
  const seedLabel = h("label", {}, "Seed:", seedInput);
  const generateButton = h(
    "button",
    {
      onclick: () => {
        clearInterval(timerId);

        ticks = 0;
        islands = new IslandGrid(WIDTH, HEIGHT, seedInput.valueAsNumber);

        timerId = setInterval(function tick() {
          islands.step();
          islands.step();
          islands.step();

          ticks += 3;
          if (islands.done) {
            clearInterval(timerId);
            islands.deepenWater();
          }

          renderIslands(islands, cx);
        }, 1000 / 30);
      },
    },
    "Generate"
  );

  renderIslands(islands, cx);

  return [canvas, seedLabel, generateButton];
}

Object.assign(globalThis, { IslandApplet });
