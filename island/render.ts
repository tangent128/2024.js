import { BEACH, DENSE_FOREST, LIGHT_FOREST, MOUNTAIN, WATER } from "./data";
import { IslandGrid } from "./grid";

export function renderIslands(
  islands: IslandGrid,
  cx: CanvasRenderingContext2D
) {
  for (let y = 0; y < islands.height; y++) {
    for (let x = 0; x < islands.width; x++) {
      const tile = islands.data[islands.xy(x, y)];
      switch (tile) {
        case -3:
        case -2:
        case -1:
          cx.fillStyle = "#000088";
          break;
        case WATER:
          cx.fillStyle = "blue";
          break;
        case BEACH:
          cx.fillStyle = "yellow";
          break;
        case LIGHT_FOREST:
          cx.fillStyle = "#00ff00";
          break;
        case DENSE_FOREST:
          cx.fillStyle = "#008800";
          break;
        case MOUNTAIN:
        case MOUNTAIN + 1:
        case MOUNTAIN + 2:
          cx.fillStyle = "#666666";
          break;
        default:
          cx.fillStyle = "#88aaff";
          break;
      }
      cx.fillRect(x, y, 1, 1);
    }
  }
}
