export type TILE = number;

export const WATER = 0;
export const BEACH = 1;
export const LIGHT_FOREST = 2;
export const DENSE_FOREST = 3;
export const MOUNTAIN = 4;
export const ICECAP = 7;
