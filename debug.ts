import { TickDebug } from "./debug/tick";
import { IdvDebug } from "./debug/idv";

Object.assign(globalThis, { TickDebug, IdvDebug });
