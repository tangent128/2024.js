export type Cancel = () => void;
export type Source<T> = {
  (): T;
  (callback: (value: T) => void): Cancel;
};
