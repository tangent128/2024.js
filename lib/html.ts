export function h<Name extends keyof HTMLElementTagNameMap>(
  name: Name,
  props: Partial<HTMLElementTagNameMap[Name]> = {},
  ...children: (Node | string)[]
): HTMLElementTagNameMap[Name] {
  const element = Object.assign(document.createElement(name), props);
  element.append(...children);
  return element;
}

export function canvas2d(
  props: Partial<HTMLCanvasElement>
): [HTMLCanvasElement, CanvasRenderingContext2D] {
  const canvas = h("canvas", props);

  const cx = canvas.getContext("2d");
  if (!cx) {
    throw new Error("2D rendering context not supported");
  }

  return [canvas, cx];
}
