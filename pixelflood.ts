// TODO choose generator
// TODO get/put image data in render/tick loop
// TODO rediscover RGB/HSV pixelflood techniques

import { canvas2d, h } from "./lib/html";

interface Controls {
  seed: HTMLInputElement;
}

function PixelfloodApplet() {
  const [canvas, cx] = canvas2d({});
  const [controls, controlUi] = ControlUi();

  return [canvas, ...controlUi];
}

function numInput(init: number) {
  return h("input", { type: "number", valueAsNumber: init });
}

function ControlUi(): [Controls, HTMLElement[]] {
  let seed, width, height;

  const html = [
    h("h2", {}, "Controls"),
    h(
      "div",
      {},
      h(
        "label",
        {},
        "Width: ",
        (width = numInput(128)),
        "Height: ",
        (height = numInput(128))
      )
    ),
    h("div", {}, h("label", {}, "Random Seed: ", (seed = numInput(128)))),
  ];

  return [{ seed }, html];
}

Object.assign(globalThis, { PixelfloodApplet });
