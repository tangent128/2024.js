import { h } from "../lib/html";
import { Idv, StringProperty } from "../lib/idv";

export function IdvDebug() {
  const textarea = h(
    "textarea",
    {
      cols: 80,
      rows: 40,
      oninput(ev) {
        parse();
      },
    },
    `# Idv Testing Ground
User: tangent128
  Uid: 10000
  Shell: tclsh
  Group: users
  Group: sudo
  Banner:
    +------------------+
    |Welcome to Debian!|
    +------------------+

User: tirga
  Uid: 10101
  Shell: bash
  Group: users
`
  );

  const pre = h("pre");

  function parse() {
    try {
      const idv = Idv.parse(textarea.value);

      pre.textContent = JSON.stringify(
        idv.getMergedMap("User", UserFromDocument),
        null,
        2
      );
    } catch (e) {
      pre.textContent = String(e);
    }
  }
  parse();

  return [textarea, pre];
}

const UserFromDocument = (idv: Idv) => {
  return {
    shell: idv.getProperty("Shell", StringProperty),
    groups: idv.getList("Group", StringProperty),
    banner: idv.getProperty("Banner", StringProperty),
  };
};
