import { h } from "../lib/html";
import { Cancel } from "../lib/source";
import { tick } from "../lib/tick";

export function TickDebug() {
  const output = h("pre", {});

  const tickSource = tick(30);
  let cancel: Cancel;

  const begin = () => {
    // reset / init
    cancel?.();
    output.innerHTML = "";
    // subscribe to source, stash cancellation func
    cancel = tickSource((tick) => {
      switch (tick[0]) {
        case "physics":
          output.append(`${new Date().toISOString()}: Physics\n`);
          break;
        case "render":
          output.append(
            `${new Date().toISOString()}: Render (dt=${tick[1]})\n`
          );
          break;
      }
    });
  };

  const end = () => cancel?.();

  const start = h("button", { onclick: begin }, "Begin");
  const stop = h("button", { onclick: end }, "Cease");

  return [h("h1", {}, "Tick Event Demo"), start, stop, output, h("hr")];
}
